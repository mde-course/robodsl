/**
 */
package br.unicamp.ic.mo631.robodsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Behavior</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getBehavior()
 * @model abstract="true"
 * @generated
 */
public interface Behavior extends EObject {
} // Behavior
