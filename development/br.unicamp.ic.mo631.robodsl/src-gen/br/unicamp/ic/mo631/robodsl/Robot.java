/**
 */
package br.unicamp.ic.mo631.robodsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Robot</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Robot#getBrand <em>Brand</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Robot#getModel <em>Model</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Robot#getBehavior <em>Behavior</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Robot#getFollowedBy <em>Followed By</em>}</li>
 * </ul>
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getRobot()
 * @model
 * @generated
 */
public interface Robot extends Element {
	/**
	 * Returns the value of the '<em><b>Brand</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Brand</em>' attribute.
	 * @see #setBrand(String)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getRobot_Brand()
	 * @model
	 * @generated
	 */
	String getBrand();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Robot#getBrand <em>Brand</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Brand</em>' attribute.
	 * @see #getBrand()
	 * @generated
	 */
	void setBrand(String value);

	/**
	 * Returns the value of the '<em><b>Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model</em>' attribute.
	 * @see #setModel(String)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getRobot_Model()
	 * @model
	 * @generated
	 */
	String getModel();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Robot#getModel <em>Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model</em>' attribute.
	 * @see #getModel()
	 * @generated
	 */
	void setModel(String value);

	/**
	 * Returns the value of the '<em><b>Behavior</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Behavior</em>' containment reference.
	 * @see #setBehavior(Behavior)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getRobot_Behavior()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Behavior getBehavior();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Robot#getBehavior <em>Behavior</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Behavior</em>' containment reference.
	 * @see #getBehavior()
	 * @generated
	 */
	void setBehavior(Behavior value);

	/**
	 * Returns the value of the '<em><b>Followed By</b></em>' reference list.
	 * The list contents are of type {@link br.unicamp.ic.mo631.robodsl.Robot}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Followed By</em>' reference list.
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getRobot_FollowedBy()
	 * @model transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='\n\t\t\t\tRobot.allInstances()\n\t\t\t\t\t-&gt;select(\n\t\t\t\t\t\tlet f : Follow = behavior.oclAsType(Follow) in \n\t\t\t\t\t\tnot f.oclIsInvalid() and f.target = self\n\t\t\t\t\t  )'"
	 *        annotation="http://www.eclipse.org/OCL/Collection nullFree='false'"
	 * @generated
	 */
	EList<Robot> getFollowedBy();

} // Robot
