/**
 */
package br.unicamp.ic.mo631.robodsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Element#getName <em>Name</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Element#getSize <em>Size</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Element#getStartingPosition <em>Starting Position</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Element#getClosestElement <em>Closest Element</em>}</li>
 * </ul>
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getElement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='PositionAndSizeMustFitScene'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot PositionAndSizeMustFitScene='\n\t\t\tsize.oclIsUndefined() or\n\t\t\tlet minX : ecore::EDouble = startingPosition.x - size.x/2 in\n\t\t\t\tlet maxX : ecore::EDouble = startingPosition.x + size.x/2 in\n\t\t\t\t\tlet minY : ecore::EDouble = startingPosition.y - size.y/2 in\n\t\t\t\t\t\tlet maxY : ecore::EDouble = startingPosition.y + size.x/2 in\n\t\t\tminX &gt;= 0 and maxX &lt;= oclContainer.oclAsType(Scene).size.x-1 and \n\t\t\tminY &gt;= 0 and maxY &lt;= oclContainer.oclAsType(Scene).size.y-1 '"
 * @generated
 */
public interface Element extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getElement_Name()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Element#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' containment reference.
	 * @see #setSize(Size)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getElement_Size()
	 * @model containment="true"
	 * @generated
	 */
	Size getSize();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Element#getSize <em>Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' containment reference.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(Size value);

	/**
	 * Returns the value of the '<em><b>Starting Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Starting Position</em>' containment reference.
	 * @see #setStartingPosition(Position)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getElement_StartingPosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Position getStartingPosition();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Element#getStartingPosition <em>Starting Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Starting Position</em>' containment reference.
	 * @see #getStartingPosition()
	 * @generated
	 */
	void setStartingPosition(Position value);

	/**
	 * Returns the value of the '<em><b>Closest Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Closest Element</em>' reference.
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getElement_ClosestElement()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='\n\t\t\t\tElement.allInstances()-&gt;excluding(self)\n\t\t\t\t  -&gt;sortedBy(\n\t\t\t\t    (startingPosition.x - self.startingPosition.x)*(startingPosition.x - self.startingPosition.x) + \n\t\t\t\t  \t(startingPosition.y - self.startingPosition.y)*(startingPosition.y - self.startingPosition.y)\n\t\t\t\t  )\n\t\t\t\t  -&gt;first()'"
	 * @generated
	 */
	Element getClosestElement();

} // Element
