/**
 */
package br.unicamp.ic.mo631.robodsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scene</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Scene#getElements <em>Elements</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Scene#getName <em>Name</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Scene#getSize <em>Size</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Scene#getArea <em>Area</em>}</li>
 * </ul>
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getScene()
 * @model
 * @generated
 */
public interface Scene extends EObject {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link br.unicamp.ic.mo631.robodsl.Element}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getScene_Elements()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Element> getElements();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getScene_Name()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Scene#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' containment reference.
	 * @see #setSize(Size)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getScene_Size()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Size getSize();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Scene#getSize <em>Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' containment reference.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(Size value);

	/**
	 * Returns the value of the '<em><b>Area</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Area</em>' attribute.
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getScene_Area()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='size.x * size.y'"
	 * @generated
	 */
	double getArea();

} // Scene
