/**
 */
package br.unicamp.ic.mo631.robodsl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Random</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getRandom()
 * @model
 * @generated
 */
public interface Random extends Behavior {
} // Random
