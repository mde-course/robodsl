/**
 */
package br.unicamp.ic.mo631.robodsl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Targeted Behavior</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.TargetedBehavior#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getTargetedBehavior()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='CannotTargetItself'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot CannotTargetItself='target &lt;&gt; oclContainer'"
 * @generated
 */
public interface TargetedBehavior extends Behavior {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Robot)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getTargetedBehavior_Target()
	 * @model required="true"
	 * @generated
	 */
	Robot getTarget();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.TargetedBehavior#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Robot value);

} // TargetedBehavior
