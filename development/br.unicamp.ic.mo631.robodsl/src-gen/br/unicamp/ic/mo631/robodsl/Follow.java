/**
 */
package br.unicamp.ic.mo631.robodsl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Follow</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getFollow()
 * @model
 * @generated
 */
public interface Follow extends TargetedBehavior {
} // Follow
