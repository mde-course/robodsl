/**
 */
package br.unicamp.ic.mo631.robodsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Size</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Size#getX <em>X</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Size#getY <em>Y</em>}</li>
 * </ul>
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getSize()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='SizeMustBePositive'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot SizeMustBePositive='x &gt; 0 and y &gt; 0'"
 * @generated
 */
public interface Size extends EObject {
	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(double)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getSize_X()
	 * @model required="true"
	 * @generated
	 */
	double getX();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Size#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(double value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(double)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getSize_Y()
	 * @model required="true"
	 * @generated
	 */
	double getY();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Size#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(double value);

} // Size
