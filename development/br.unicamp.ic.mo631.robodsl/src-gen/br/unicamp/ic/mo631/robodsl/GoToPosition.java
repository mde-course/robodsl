/**
 */
package br.unicamp.ic.mo631.robodsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Go To Position</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.GoToPosition#getPosition <em>Position</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.GoToPosition#isLoop <em>Loop</em>}</li>
 * </ul>
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getGoToPosition()
 * @model
 * @generated
 */
public interface GoToPosition extends Behavior {
	/**
	 * Returns the value of the '<em><b>Position</b></em>' containment reference list.
	 * The list contents are of type {@link br.unicamp.ic.mo631.robodsl.Position}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' containment reference list.
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getGoToPosition_Position()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Position> getPosition();

	/**
	 * Returns the value of the '<em><b>Loop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop</em>' attribute.
	 * @see #setLoop(boolean)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getGoToPosition_Loop()
	 * @model required="true"
	 * @generated
	 */
	boolean isLoop();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.GoToPosition#isLoop <em>Loop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop</em>' attribute.
	 * @see #isLoop()
	 * @generated
	 */
	void setLoop(boolean value);

} // GoToPosition
