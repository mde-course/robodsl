package br.unicamp.ic.mo631.robodsl.examples.ui.handlers;

import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import br.unicamp.ic.mo631.robodsl.Element;
import br.unicamp.ic.mo631.robodsl.RobodslFactory;
import br.unicamp.ic.mo631.robodsl.RobodslPackage;
import br.unicamp.ic.mo631.robodsl.Robot;
import br.unicamp.ic.mo631.robodsl.RunAway;
import br.unicamp.ic.mo631.robodsl.Scene;
import br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl;

import org.eclipse.jface.dialogs.MessageDialog;

/**
 * This class is an example of how the Ecore APIs of a given metamodel
 * (generated metamodel implementation) can be used to manipulate models.
 * 
 * This example is for the PLUGIN MODE, that is, an Eclipse plugin.
 * 
 * This class is the handler for a button that will appear in the menu
 * of the new Eclipse instance. The definition of menu and buttons is done in
 * the file "plugin.xml"
 */
public class PluginHandlerExample extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		
		// Show message window
		MessageDialog.openInformation(
				window.getShell(),
				"RoboDSL Usage - Example Plugin",
				"Reading and modifying file '/examples/lab01.robodsl'");
		
		// Read and manipulate the model
		readAndModifyModel();

		// Show message window
		MessageDialog.openInformation(
				window.getShell(),
				"RoboDSL Usage - Example Plugin",
				"Model modified and saved as '/examples/lab01-modified.robodsl'");
		return null;
	}

	/*
	 * This function reads an existing model from the "robodsl" metamodel,
	 * alters it, and saves it as a new XMI file (with ".robodsl" extension).
	 * 
	 * It expects a file named "lab01.robodsl" in a project named "examples" in the workspace,
	 * and it will create a file named "lab01-modified.robodsl" in the same place
	 */
	private static void readAndModifyModel() {
		// Obtain the instance of the package implementing the metamodel
		RobodslPackage pack = RobodslPackageImpl.eINSTANCE;
		// Obtain the factory (for creating instance of metaclasses)
		RobodslFactory factory = pack.getRobodslFactory();
		
		// Create a ResourceSet, that is, a container for resources.
		// In plugin mode we don't need further registrations
		ResourceSet rs = new ResourceSetImpl();

		// Read a resource (XMI file)
		Resource res = rs.getResource(URI.createURI("/examples/lab01.robodsl"), true);
		
		// Get the Scene instance
		// (we know that it is the top-level element)
		Scene exp2 = (Scene)res.getContents().get(0);
		
		// 1) Change the name of the Scene to represent EXP3
		exp2.setName("EXP3");
		
		// 2) Find the two robots (instances of the Robot metaclass)
		Robot r2 = null;
		Robot r3 = null;
		for(Element el : exp2.getElements()) {
			if(el.getName().equals("R2")) {
				r2 = (Robot)el;
			}else if(el.getName().equals("R3")) {
				r3 = (Robot)el;
			}
		}
	
		// 3) Change the information of R3 (model and brand)
		r2.setBrand("SoftBank Robotics");
		r2.setModel("NAO");
		
		// 4) Change the behavior or R2
		// (create a new RunAway behavior and replace the old one)
		RunAway r3behavior = factory.createRunAway();
		r3behavior.setTarget(r2);
		r3.setBehavior(r3behavior);
		
		// Create a new resource (XMI file)
		Resource rMod = rs.createResource(URI.createURI("/examples/lab01-modified.robodsl"));
		
		// Add the scene to the new resource
		rMod.getContents().add(exp2);
		
		// Save the new resource to disk
		// (may throw a IOException)
		try {
			rMod.save(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
