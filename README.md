# RoboDSL Metamodel

## Info

This repository contains a set of examples for the "Model-Driven Software Engineering" course at IC/UNICAMP.  
The examples are meant to run with **Eclipse Modeling Tools**, version **2020-06**

## Workspaces

The folders **development** and **runtime** folders are meant to be used as Eclipse workspaces. They contain the development and runtime (second Eclipse instance) projects, respectively.

## Development

The **development** workspace contains the following projects.

_Plugins containing the "robodsl" metamodel and its implementation, as well as the generated editor:_

- br.unicamp.ic.mo631.robodsl
- br.unicamp.ic.mo631.robodsl.edit
- br.unicamp.ic.mo631.robodsl.editor

_Acceleo generator to generate SVG images from RoboDSL models:_

- br.unicamp.ic.mo631.robodsl.gen.svg

_Examples on how to read and write model instances programmatically, that is, using the generated API:_

- br.unicamp.ic.mo631.robodsl.examples
- br.unicamp.ic.mo631.robodsl.examples.ui

## Runtime

The **runtime** workspace contains the following projects:

_Example instances of the "robodsl" metamodel_

- examples

## Setup

1. Clone or download the repository
2. Open Eclipse using the _development_ folder as workspace
3. Import the projects from the _development_ folder (_File / Import... / Existing Projects into Workspace_)
4. Run the launch configuration _"Eclipse Application (new Eclipse instance).launch"_
5. Import the projects from the _runtime_ folder (_File / Import... / Existing Projects into Workspace_)
